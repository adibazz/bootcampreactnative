// Nomor 1
var naik = " - I love coding";
var turun = " - I will become a mobile developer";
var number = 2;
var changes = 2;
var teks = naik;

console.log("LOOPING PERTMA");
while (number > 0 && number < 22) {
  console.log(number + teks);
  number+=changes;
  if (number > 20) {
    number = 20;
    console.log("LOOPING KEDUA");
    changes = -changes;
    teks = turun;
  }
}
// Nomor 2
console.log("");
var ganjil = " - Santai";
var genap = " - Berkualitas";
var gabung = " - I Love Coding"
var teks2;
for (var nomor = 1; nomor<=20; nomor++) {
  if (nomor%2 !== 0 && nomor%3 === 0) teks2 = gabung;
  else if (nomor%2 !== 0) teks2 = ganjil;
  else teks2 = genap;
  console.log(nomor + teks);
}

// Nomor 3
console.log("");
var panjang = 8;
var lebar = 4;
for (var angka1 = 0; angka1 < lebar; angka1++) {
  var pagar = "";
  for (var angka2 = 0; angka2 < panjang; angka2++) {
    pagar += "#";
  }
  console.log(pagar);
}
// Nomor 4
console.log("");
for (var j = 0; j <= 7; j++) {
  var segitiga = "";
  for (var k = 0; k < j; k++) {
    segitiga += "#";
  }
  console.log(segitiga);
}
// Nomor 5
console.log("");
for (var a = 1; a <= 8; a++) {
  var pola = "";
  if (a%2 !== 0){
    for (var b = 1; b <= 8; b++) {
      if (b%2 !== 0) pola += " ";
      else pola += "#";
    }
    console.log(pola);
  }
  else {
    for (var c = 1; c <= 8; c++) {
      if (c%2 !== 0) pola += "#";
      else pola += " ";
    }
    console.log(pola);
  }
}