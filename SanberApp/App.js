import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AboutScreen from './Tugas/Tugas13/AboutScreen';



export default function App() {
  return (
    <AboutScreen/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});