import React from 'react'
import { View, Text, ScrollView, StyleSheet, Image } from 'react-native'

export default function AboutScreen() {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={{marginTop: 25, fontSize: 24}}>Tentang Saya</Text>
                <Image
                    style={styles.avatar}
                    source={require('./asset/profile.jpg')}/>
                <Text style={{fontSize: 24, fontWeight: 'bold'}}>John Doe</Text>
                <Text style={{fontSize: 18}}>React Native Developer</Text>
                <View style={styles.grayContainer}>
                    <View style={styles.boxTitle}>
                        <Text style={styles.textTitle}>Portofolio</Text>
                    </View>
                    <View style={styles.lineSeparator}></View>
                    <View style={styles.contentItem}>
                        <View style={{alignItems: 'center'}}>
                            <View style={styles.imageWrapper}>
                                <Image source={require('./asset/gitlab.png')}/>
                            </View>
                            <Text style={{fontWeight: 'bold'}}>@sanbercode</Text>
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <View style={styles.imageWrapper}>
                                <Image source={require('./asset/github.png')}/>
                            </View>
                            <Text style={{fontWeight: 'bold'}}>@sanbercode</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.grayContainer}>
                    <View style={styles.boxTitle}>
                        <Text style={styles.textTitle}>Contacts</Text>
                    </View>
                    <View style={styles.lineSeparator}></View>
                    <View style={styles.contentItem}>
                        <View style={{alignItems: 'center'}}>
                            <View style={styles.imageWrapper}>
                                <Image 
                                    style={{width: 50, height: 50}}
                                    source={require('./asset/linkedin.png')}/>
                            </View>
                            <Text style={{fontWeight: 'bold'}}>@Sanbercode</Text>
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <View style={styles.imageWrapper}>
                                <Image
                                    style={{width: 50, height: 50}}
                                    source={require('./asset/email.png')}/>
                            </View>
                            <Text style={{fontWeight: 'bold'}}>info@sanbercode.com</Text>
                        </View>
                    </View>
                    <View style={styles.contentItem}>
                    <View style={{alignItems: 'center'}}>
                            <View style={styles.imageWrapper}>
                                <Image
                                    style={{width: 50, height: 50}}
                                    source={require('./asset/wa.png')}/>
                            </View>
                            <Text style={{fontWeight: 'bold'}}>081234567890</Text>
                        </View>
                    </View>
                </View>
                <View style={{marginBottom: 12}}></View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white',
        alignItems: 'center',
        paddingTop: 30
    },
    avatar: {
        width: 200,
        height: 200,
        borderRadius: 50
    },
    grayContainer:{
        marginTop: 8,
        backgroundColor: '#3EC6FF',
        borderWidth: 2,
        width: 300,
        borderRadius: 10,
        borderColor: '#003366'
    },
    boxTitle: {
        backgroundColor: '#003366',
        borderRightWidth: 2,
        borderBottomWidth: 2,
        width: 100,
        borderTopLeftRadius: 6,
        borderBottomRightRadius: 10,
        borderColor: '#003366'
    },
    textTitle: {
        color: 'white',
        marginLeft: 8,
        fontSize: 18
    },
    lineSeparator: {
        borderBottomWidth: 2,
        borderBottomColor: '#003366',
        margin: 4
    },
    contentItem: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginBottom: 8
    },
    imageWrapper: {
        width: 70,
        height: 70,
        borderRadius: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    }
})
