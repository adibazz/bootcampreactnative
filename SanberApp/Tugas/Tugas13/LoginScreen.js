import React from 'react'
import {View, Text, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView} from 'react-native'

export default function LoginScreen() {
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image source={require('./asset/logo.png')}/>
                </View>
                <View style={styles.card}>
                    <Text style={{fontSize: 24, color: 'white', marginTop: 15, marginBottom: 20, alignItems: 'center', marginBottom: 10}}>Login</Text>
                    <View style={styles.inputSection}>
                        <Text style={styles.inputTitle}>Username</Text>
                        <TextInput
                            style={styles.inputText}
                            placeholder="Type your username"/>
                    </View>
                    <View style={styles.inputSection}>
                        <Text style={styles.inputTitle}>Password</Text>
                        <TextInput
                            style={styles.inputText}
                            placeholder="Type your password"/>
                    </View>
                </View>
                <TouchableOpacity style={styles.buttonBlue}>
                    <Text style={{fontSize: 18}}>Masuk</Text>
                </TouchableOpacity>
                <View style={styles.registerSection}>
                    <Text>Belum memiliki akun?</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{marginRight: 4, marginTop: 12}}>Segera</Text>
                        <TouchableOpacity style={styles.buttonWhite}>
                            <Text style={{color:'#3EC6FF'}}>Daftar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white',
        alignItems: 'center'
    },
    logoContainer: {
        alignItems: 'center',
        height: 100,
        paddingTop: 30,
    },
    card: {
        alignItems: 'center',
        marginTop: 74,
        marginHorizontal: 16,
        height: 245,
        backgroundColor: '#003366',
        borderRadius: 20,
    },
    inputSection: {
        paddingHorizontal: 8,
        height: 65,
        marginTop: 11
    },
    inputTitle: {
        fontSize: 18,
        color: 'white',
        fontStyle: 'italic',
        fontWeight:'bold'
    },
    inputText: {
        borderRadius: 10,
        backgroundColor: 'white',
        width: 300,
        height: 38,
        paddingHorizontal: 8
    },
    buttonBlue:{
        marginTop: 19,
        width: 123,
        height: 46,
        backgroundColor: '#3EC6FF',
        borderColor: '#3EC6FF',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    buttonWhite:{
        marginTop: 19,
        width: 123,
        height: 46,
        borderColor: '#3EC6FF',
        borderWidth: 2,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    registerSection: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 35
    }
})