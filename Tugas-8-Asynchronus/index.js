const readBooks = require('./callback.js');
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];

let time = 10000;
let i =  0;
function reading() {
    readBooks(time, books[i], (timeLeft) => {
        i++;
        time = timeLeft;
        if (i<books.length) reading();
    });
}
reading();