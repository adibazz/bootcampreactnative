let readBooksPromise = require('./promise.js');
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
];
async function reading() {
    let time = 10000;
    for (var i=0; i<books.length; i++) {
        time = await readBooksPromise(time, books[i]).then((timeLeft) => {
            return timeLeft;
        }).catch((timeLeft) => {
            return timeLeft;
        });
    }
}
reading();