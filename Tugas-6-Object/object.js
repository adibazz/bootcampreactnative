// Nomor 1
function arrayToObject(arr) {
    var objectCreated = {};
    var now = new Date();
    var thisYear = now.getFullYear();
    if (arr == null) return console.log("");
    for (var o=0; o<arr.length; o++) {
        objectCreated.firstName = arr[o][0];
        objectCreated.lastName = arr[o][1];
        objectCreated.gender = arr[o][2];
        objectCreated.age = ((arr[o][3] == null) || (arr[o][3] > thisYear)) ? "Invalid Birth Year" : thisYear - arr[o][3];
        var cetak = `${o+1}. ${objectCreated.firstName} ${objectCreated.lastName} :`;
        console.log(cetak);
        console.log(objectCreated);
    }
}

var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
];
arrayToObject(people);

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
];
arrayToObject(people2);

arrayToObject([]);

// Nomor 2
console.log("------------------------------------------");
function shoppingTime(memberId, money) {
    var discount = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["Casing Handphone", 50000]
    ];
    var checkOut = {};
    var listPurchased = [];
    checkOut.memberId = memberId;
    checkOut.money = money;
    if (memberId == null || memberId == "") return "Mohon maaf, toko X hanya berlaku untuk member saja";
    else if (money < 50000) return "Mohon maaf, uang tidak cukup";
    else {
        for (var i=0; i<discount.length; i++) {
            if (money >= discount[i][1]) {
                listPurchased.push(discount[i][0]);
                money -= discount[i][1];
            }
        }
        checkOut.listPurchased = listPurchased;
        checkOut.changeMoney = money;
        return checkOut;
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
// Nomor 3
console.log("------------------------------------------");
function naikAngkot(arrPenumpang) {
    var penumpang = [];
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    for (var p=0; p<arrPenumpang.length; p++) {
        var detailPenumpang = {};
        detailPenumpang.penumpang = arrPenumpang[p][0];
        detailPenumpang.naikDari = arrPenumpang[p][1];
        detailPenumpang.tujuan = arrPenumpang[p][2];
        var titikNaik = 0;
        var titikTurun =  0;
        for (var q=0; q<rute.length; q++) {
            if (rute[q] == detailPenumpang.naikDari) titikNaik = q;
            else if (rute[q] == detailPenumpang.tujuan) titikTurun = q;
        }
        detailPenumpang.bayar = (titikNaik < titikTurun) ? ((titikTurun-titikNaik)*2000) : ((titikNaik-titikTurun)*2000);
        penumpang.push(detailPenumpang);
    }
    return penumpang;
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));