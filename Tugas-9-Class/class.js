// Nomor 1
// //Release 0
console.log("");
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
}
var sheep = new Animal("Shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
// //Release 1
console.log("");
class Ape extends Animal {
    constructor(name, legs = 2) {
        super(name);
        this.legs = legs;
    }
    yell() {
        return console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name, legs) {
        super(name, legs);
    }
    jump() {
        return console.log("hop hop");
    }
}
var sungokong = new Ape("kera sakti");
sungokong.yell();
var kodok = new Frog("buduk");
kodok.jump();
// Nomor 2
console.log("");
class Clock {
    constructor({template}) {
        this.template = template.toString();
    }
    render() {
        var date = new Date();
        
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
        
        console.log(output);
    }
    stop() {
        clearInterval(this.timer)
    }
    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();