// Nomor 1
function range(startNum, finishNum) {
    var hasil = [];
    if (startNum == null || finishNum == null) return -1;
    else if (finishNum < startNum) {
        for (var a = startNum; a >= finishNum; a--) {
            hasil.push(a);
        }
        return hasil;
    }
    for (var a = startNum; a <= finishNum; a++) {
        hasil.push(a);
    }
    return hasil;
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());

// Nomor 2
console.log("");
function rangeWithStep(startNum2, finishNum2, step) {
    var hasil2 = [];
    if (startNum2 == null || finishNum2 == null) return -1;
    else if (finishNum2 < startNum2) {
        for (var b = startNum2; b >= finishNum2; b -= step) {
            hasil2.push(b);
        }
        return hasil2;
    }
    for (var b = startNum2; b <= finishNum2; b += step) {
        hasil2.push(b);
    }
    return hasil2;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

// Nomor 3
console.log("");
function sum(startNum3, finishNum3, step2) {
    var hasil3 = [];
    if (finishNum3 == null && startNum3 != null) return startNum3;
    if (step2 == null) step2 = 1;
    if (startNum3 > finishNum3) [finishNum3, startNum3] = [startNum3, finishNum3];
    for (var a = startNum3; a <= finishNum3; a += step2) {
        hasil3.push(a);
    }
    return hasil3.reduce((a, b) => a + b, 0);
}
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

// Nomor 4
console.log("");
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
function dataHandling(input) {
    input.forEach(function(data) {
        console.log("Nomor ID: " + data[0]);
        console.log("Nama Lengkap: " + data[1]);
        console.log(`TTL: ${data[2]} ${data[3]}`);
        console.log("Hobi: " + data[4]);
        console.log("");
    });
}
dataHandling(input);

// Nomor 5
console.log("");
function balikKata(kata) {
    var balik = ""
    for (var a = kata.length-1; a >= 0; a--) {
        balik += kata[a];
    }
    return balik;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

// Nomor 6
console.log("");
function dataHandling2(input2) {
    // Menambah nama belakang
    var name = input2[1].split(" ");
    name.push("Elsharawy");
    name = name.join(" ");
    input2.splice(1,1,name);

    // Mengubah nama provinsi
    var provinsi = input2[2].split(" ");
    if (provinsi[0] !== "Provinsi") provinsi.unshift("Provinsi");
    provinsi = provinsi.join(" ");
    input2.splice(2,1,provinsi);

    // Menambah jenis kelamni dan SMA asal
    input2.splice(input2.length-1,1,"Pria","SMA Internasional Metro");
    console.log(input2);

    // Menampilan bulan lahir
    var lahir = input2[3].split("/");
    bulan = findMonth(lahir[1]);
    console.log(bulan);
    
    // Menampilkan tangal lahir descending
    lahir.sort(function (value1, value2) {return value2 - value1});
    console.log(lahir);

    // Menampilkan tanggal lahir
    var lahir2 = input2[3].split("/");
    lahir2 = lahir2.join("-");
    console.log(lahir2);

    // Menampilkan nama 15 karakter
    name = name.slice(0,14);
    console.log(name);

}
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);

function findMonth(bulan) {
    switch(bulan) {
        case "01":
            bulan = "Januari";
            break;
        case "02":
            bulan = "Februari";
            break;
        case "03":
            bulan = "Maret";
            break;
        case "04":
            bulan = "April";
            break;
        case "05":
            bulan = "Mei";
            break;
        case "06":
            bulan = "Juni";
            break;
        case "07":
            bulan = "Juli";
            break;
        case "08":
            bulan = "Agustus";
            break;
        case "09":
            bulan = "September";
            break;
        case "10":
            bulan = "Oktober";
            break;
        case "11":
            bulan = "November";
            break;
        case "12":
            bulan = "Desember";
            break;
        default:
            bulan = ""
    }
    return bulan;
}