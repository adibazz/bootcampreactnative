// if-else
var nama = "Budi";
var peran = "Guard"; 

console.log("Nama: " + nama);
console.log("Peran: " + peran);

console.log("Selamat datang di Dunia Werewolf, " + nama);

if (peran == "Penyihir" || peran == "penyihir") console.log("Halo " + peran + " " + nama + " , Kamu dapat melihat siapa yang menjadi werewolf!");
else if (peran == "Guard" || peran == "guard") console.log("Halo " + peran + " " + nama + ", Kamu akan membantu melindungi temanmu dari serangan werewolf.");
else if (peran == "Warewolf" || peran == "guard") console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!");
else console.log("Maaf " + nama + ", peran " + peran + " tidak ada dalam permainan ini.");

// Swwitch Case
var date = 22;
var month = 11;
var year = 1999;
var bulan;

switch(month) {
    case 1: {
        bulan = "Januari";
        break;
    }
    case 2: {
        bulan = "Februari";
        break;
    }case 3: {
        bulan = "Maret";
        break;
    }
    case 4: {
        bulan = "April";
        break;
    }
    case 5: {
        bulan = "Mei";
        break;
    }
    case 6: {
        bulan = "Juni";
        break;
    }
    case 7: {
        bulan = "Juli";
        break;
    }
    case 8: {
        bulan = "Agustus";
        break;
    }
    case 9: {
        bulan = "September";
        break;
    }
    case 10: {
        bulan = "Oktober";
        break;
    }
    case 11: {
        bulan = "November";
        break;
    }
    case 12: {
        bulan = "Desember";
        break;
    }
    default: bulan="";
}

console.log(date + " " + bulan + " " + year);